<?php

namespace Drupal\parse_news\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\parse_news\Model\NewsModel;

/**
 * Class NewsController.
 *
 * @package Drupal\parse_news\Controller.
 */
class NewsController extends ControllerBase {

  /**
   * @var \Drupal\parse_news\Model\NewsModel
   */
  private $newsModel;

  /**
   * {@inheritdoc}
   */
  public function __construct(NewsModel $newsModel) {
    $this->newsModel = $newsModel;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('parse_news.model')
    );
  }

  /**
   * List of the all news.
   *
   * @return array
   *   List of news.
   */
  public function newsListBuilder() {

    $header = [
      ['data' => $this->t('Title'), 'field' => 'title', 'sort' => 'asc'],
      ['data' => $this->t('Date'), 'field' => 'created'],
      ['data' => $this->t('Date Parse'), 'field' => 'news_date_from'],
      ['data' => $this->t('Action')],
    ];

    $result = $this->newsModel->loadHeader($header);

    if (!$result) {
      drupal_set_message($this->t('There are not any news in database!'));
    }

    $rows = [];
    foreach ($result as $row) {

      $node = $this->newsModel->getEntity('node')->load($row->nid);

      $date = $node->get('news_date_from')->value;

      $path      = '/admin/content/bd_news/edit/' . $row->nid;
      $edit      = Url::fromUri('internal:' . $path);
      $edit_link = Link::fromTextAndUrl(t('Edit'), $edit)->toString();

      $path     = '/admin/content/bd_news/delete/' . $row->nid;
      $del      = Url::fromUri('internal:' . $path);
      $del_link = Link::fromTextAndUrl(t('Delete'), $del)->toString();

      $path       = '/news/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $mainLink = $this->t('@edit | @delete', [
        '@edit'   => $edit_link,
        '@delete' => $del_link,
      ]);

      $rows[] = [
        ['data' => $title_link],
        ['data' => date('Y-m-d', $row->created)],
        ['data' => $date],
        ['data' => $mainLink],
      ];
    }

    $build = [
      '#markup' => $this->t('News List'),
    ];

    // Generate the table.
    $build['config_table'] = [
      '#theme'  => 'table',
      '#header' => $header,
      '#rows'   => $rows,
    ];

    // Finally add the pager.
    $build['pager'] = [
      '#type' => 'pager',
    ];

    return $build;

  }

  /**
   * Delete News.
   *
   * @param int $id
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   */
  public function deleteNews($id) {
    $this->newsModel->delete($id);

    drupal_set_message($this->t('News deleted!'));
    return $this->redirect('parse_news.list');
  }

  /**
   * Data for the archive news view.
   */
  public function archive() {
    $items = [];

    $result = $this->newsModel->loadNews();

    foreach ($result as $row) {
      $node = $this->newsModel->getEntity('node')->load($row->nid);

      $path       = '/news/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $date = $node->get('news_date_from')->value;

      $image = $node->get('field_image')->getValue();
      $src   = $this->newsModel->getImgUrl($image);

      $tagsIds = $node->get('parse_news_field_related')->getValue();
      $tagsObj = $this->newsModel->getEditRelatedValues($tagsIds);

      $tagNames = [];

      foreach ($tagsObj as $tag) {
        $pathTag      = '/news/category/' . strtolower($tag->getName());
        $titleTag     = Url::fromUri('internal:' . strtolower($pathTag));
        $titleLinkTag = Link::fromTextAndUrl($tag->getName(), $titleTag)
                            ->toString();
        $tagNames[]   = $titleLinkTag;
      }

      $items[] = [
        'title'      => $title_link,
        'date_parse' => $date,
        'image_src'  => $src,
        'tags'       => $tagNames,
      ];
    }

    return [
      '#theme' => 'newsarchive',
      '#items' => $items,
    ];
  }

  public function archiveTag($tag) {
    $items = [];

    $result = $this->newsModel->loadNews(NULL, $tag);

    foreach ($result as $row) {
      $node = $this->newsModel->getEntity('node')->load($row->nid);

      $path       = '/news/' . $row->nid;
      $title      = Url::fromUri('internal:' . $path);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $date = $node->get('news_date_from')->value;

      $image = $node->get('field_image')->getValue();
      $src   = $this->newsModel->getImgUrl($image);

      $tagsIds = $node->get('parse_news_field_related')->getValue();
      $tagsObj = $this->newsModel->getEditRelatedValues($tagsIds);

      $tagNames = [];

      foreach ($tagsObj as $tag) {
        $pathTag      = '/news/category/' . $tag->getName();
        $titleTag     = Url::fromUri('internal:' . strtolower($pathTag));
        $titleLinkTag = Link::fromTextAndUrl($tag->getName(), $titleTag)
                            ->toString();
        $tagNames[]   = $titleLinkTag;
      }

      $items[] = [
        'title'      => $title_link,
        'date_parse' => $date,
        'image_src'  => $src,
        'tags'       => $tagNames,
      ];
    }

    return [
      '#theme' => 'newsarchive',
      '#items' => $items,
    ];

}

  /**
   * Data for the single news view.
   *
   * @param int $id
   *   ID News.
   *
   * @return array
   */
  public function page($id) {
    $items = [];

    $news = $this->newsModel->getEditValue($id);

    $items['item'] = [
      'title'          => $news['title'],
      'news_date_from' => $news['news_date_from'],
      'body'           => $news['body'],
      'image'          => $news['img_src'],
    ];


    return [
      '#theme' => 'newspage',
      '#items' => $items,
    ];
  }

  /**
   * Block Ajax callback.
   *
   * @param int $id
   *   ID News.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   */
  public function excerptAjax($id) {

    $response = new AjaxResponse();

    $response->addCommand(new HtmlCommand('.insert', ''));

    $excerptLenght = \Drupal::config('parse_news.settings')
                            ->get('parse_news.excerpt_lenght');

    $news = $this->newsModel->getEditValue($id);

    $excerpt = $news['body'];
    $excerpt = mb_substr($excerpt, 0, $excerptLenght, 'utf-8');

    $response->addCommand(new HtmlCommand('.insert' . $id, $excerpt));

    return $response;

  }

}
