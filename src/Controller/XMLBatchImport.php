<?php

namespace Drupal\parse_news\Controller;

use Drupal\node\Entity\Node;
use Drupal;

/**
 * Class XMLBatchImport.
 *
 * @package Drupalcustom_Csv_Import
 */
class XMLBatchImport {

  private $config;

  /**
   * Batch data.
   *
   * @var array
   */
  private $batch;

  /**
   * Field data.
   *
   * @var array
   */
  private $data = [];

  /**
   * News from target resource.
   *
   * @var string
   */
  private $source;

  /**
   * HTTP of HTTPS.
   *
   * @var string
   */
  private $protocol;

  /**
   * Node type.
   *
   * @var string
   */
  private $nodeType;

  /**
   * Used date format to save in database for
   * compare and prevent upload news twice.
   *
   * @var string
   */
  private $dateFormat;

  /**
   * {@inheritdoc}
   */
  public function __construct($isBatch = 'batch', $batch_name = 'Custom XML import') {

    $this->config = Drupal::config('parse_news.settings');

    $this->source     = file_get_contents($this->config->get('parse_news.parser_feed'));
    $this->protocol   = parse_url($this->config->get('parse_news.parser_feed'), PHP_URL_SCHEME);
    $this->dateFormat = $this->config->get('parse_news.date_format');
    $this->nodeType   = $this->config->get('parse_news.node_type');
    $this->isBatch    = 'batch' === $isBatch ? TRUE : FALSE;
    if ($this->isBatch) {
      $this->batch = [
        'title'    => $batch_name,
        'finished' => [$this, 'finished'],
        'file'     => drupal_get_path('module', 'parse_news') . '/src/Controller/' . $this->getClassName() . '.php',
      ];
    }
    $this->parseXML();
  }

  /**
   * Returns Class Name without Namespace.
   *
   * @return string
   *   ClassName.
   */
  public function getClassName() {
    $nameSpace = get_class($this);
    $class     = explode('\\', $nameSpace);
    return end($class);
  }

  /**
   * Start parsing.
   *
   * Use SimpleXmlElement.
   */
  public function parseXML() {

    $x = new \SimpleXmlElement($this->source);

    $yesterdayStamp = mktime(0, 0, 0, date("m"), date("d") - 1, date("Y"));

    foreach ($x->channel->item as $entry) {

      $dataDateString      = strip_tags((string) $entry->pubDate);
      $resource['dateObj'] = \DateTime::createFromFormat($this->dateFormat, $dataDateString);
      // Data for checking start of parsing.
      $m = $resource['dateObj']->format("m");
      $d = $resource['dateObj']->format("d");
      $Y = $resource['dateObj']->format("Y");

      $resourceStamp = mktime(0, 0, 0, $m, $d, $Y);

      if ($resourceStamp > $yesterdayStamp && !$this->isNode($resource['dateObj'])) {

        $resource['title']   = strip_tags((string) $entry->title);
        $resource['content'] = strip_tags((string) $entry->children('content', TRUE)->encoded);
        $resource['img_url'] = $this->protocol . ':' . strip_tags((string) $entry->enclosure->attributes()->url);

        if ($this->isBatch) {
          $this->setOperation($resource);
        }
        else {
          $this->data['date_obj'] = $resource['dateObj'];
          $this->data['title']    = $resource['title'];
          $this->data['content']  = $resource['content'];
          $this->data['img_url']  = $resource['img_url'];
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function setOperation($entry) {
    $this->batch['operations'][] = [[$this, 'processItem'], $entry];

  }

  /**
   * Returns data status.
   *
   * @return bool
   *   Returns TRUE when data is no empty FALSE otherwise.
   */
  public function getData() {
    return !empty($this->data);
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($dateObj, $title, $content, $img_url, &$context) {
    $this->data['date_obj'] = $dateObj;
    $this->data['title']    = $title;
    $this->data['content']  = $content;
    $this->data['img_url']  = $img_url;
    $node                   = $this->process();
    $context['results'][]   = $node->id() . ' : ' . $node->label();
    $context['message']     = $node->label();
  }

  /**
   * Add new Nodes.
   *
   * @return \Drupal\Core\Entity\EntityInterface|static
   */
  public function process() {
    if (empty($this->data)) {
      $message = 'No new news:(';
      drupal_set_message($message);
      return '';
    }
    $dateWrite = $this->data['date_obj']->format($this->dateFormat);

    $image    = file_get_contents($this->data['img_url']);
    $fileName = basename($this->data['img_url']);

    $directory = 'public:// ';
    $file      = file_save_data($image, $directory . $fileName, FILE_EXISTS_REPLACE);
    $fid       = $file->id();

    $newNode = [
      'type'           => $this->nodeType,
      'title'          => $this->data['title'],
      'status'         => 1,
      'body'           => $this->data['content'],
      'news_date_from' => $dateWrite,
      'field_image'    => [
        'target_id' => $fid,
      ],
    ];

    $node = Node::create($newNode);
    $node->save();
    if ($this->isBatch) {
      return $node;
    }
  }

  /**
   * Check is in db node with that date.
   *
   * @param $date
   *
   * @return bool
   *   Return TRUE if node is and FALSE otherwise.
   */
  public function isNode($date) {
    $query = Drupal::entityQuery('node');
    $query->condition('type', $this->nodeType);
    $query->condition('news_date_from', $date->format($this->dateFormat), '=');
    $results = $query->execute();

    if (!empty($results)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setBatch() {
    batch_set($this->batch);
  }

  /**
   * {@inheritdoc}
   */
  public function processBatch() {
    batch_process();
  }

  /**
   * {@inheritdoc}
   */
  public function finished($success, $results, $operations) {
    if ($success) {
      $message = Drupal::translation()
                       ->formatPlural(count($results), 'One post processed.', '@count posts processed.');
    }
    else {
      $message = t('Finished with an error.');
    }
    drupal_set_message($message);
  }

}
