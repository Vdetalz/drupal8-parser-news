<?php

namespace Drupal\parse_news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\taxonomy\Entity\Term;

/**
 * Class EditForm.
 *
 * @package Drupal\parse_news\Form
 */
class EditForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {

    if (NULL !== $id) {
      $query = \Drupal::service('parse_news.model');
      $news  = $query->getEditValue($id);
      $terms = $query->getEditRelatedValues($news['parse_news_field_related']);
    }

    $form['title'] = [
      '#type'          => 'textfield',
      '#description'   => $this->t('Edit News Title'),
      '#title'         => $this->t('News title'),
      '#default_value' => $news ? $news['title'] : '',
      '#required'      => TRUE,
    ];

    $form['news_date_from'] = [
      '#title'         => $this->t('News Date'),
      '#description'   => $this->t('Edit News Date from parse'),
      '#default_value' => $news ? $news['news_date_from'] : '',
      '#type'          => 'textfield',
    ];

    $form['body'] = [
      '#title'         => $this->t('News'),
      '#description'   => $this->t('Edit News'),
      '#default_value' => $news ? $news['body'] : '',
      '#type'          => 'textarea',
    ];

    $form['parse_news_field_related'] = [
      '#title'              => $this->t('Taxonomy'),
      '#description'        => $this->t('Add Term'),
      '#default_value'      => isset($terms) ? $terms : '',
      '#type'               => 'entity_autocomplete',
      '#target_type'        => 'taxonomy_term',
      '#tags'               => TRUE,
      '#selection_settings' => [
        'auto_create'    => TRUE,
        'target_bundles' => ['parse_news_category'],
      ],
    ];

    $form['image'] = [
      '#markup' => '<img src="' . $news['img_src'] . '"/ >',
    ];

    $form['file'] = [
      '#title'              => $this->t('Image'),
      '#description'        => $this->t('Upload image'),
      '#type'               => 'managed_file',
      '#progress_indicator' => 'bar',
      '#progress_message'   => 'Uploading...',
      '#size'               => 8,
      '#upload_location'    => 'public://files/',
    ];

    $form['actions']['#type']  = 'actions';
    $form['actions']['submit'] = [
      '#type'        => 'submit',
      '#value'       => $news ? $this->t('Update News') : $this->t('Add News'),
      '#button_type' => 'primary',
    ];

    if ($news) {
      $form['id'] = [
        '#type'  => 'value',
        '#value' => $news['nid'],
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $title                    = $form_state->getValue('title');
    $date                     = $form_state->getValue('news_date_from');
    $body                     = $form_state->getValue('body');
    $val                      = $form_state->getValue('id');
    $file                     = $form_state->getValue('file');
    $parse_news_field_related = $form_state->getValue('parse_news_field_related');

    if (isset($file[0])) {
      $fid       = $file[0];
      $file_save = File::load($fid);
      $file_save->setPermanent();
      $file_save->save();
    }

    /*Edit News*/
    if (isset($val)) {
      $node = Node::load($val);
      $node->set('title', $title);
      $node->set('news_date_from', $date);
      $node->set('body', $body);
      $node->set('parse_news_field_related', $parse_news_field_related);

      if (isset($fid)) {
        $field_image = [
          'target_id' => $fid,
        ];
        $node->set('field_image', $field_image);
      }

      $node->save();
      drupal_set_message(t('News saved!'));
    } /*Add new data.*/
    else {
      $type = $this->config('parse_news.settings')
                   ->get('parse_news.node_type');
      $node = Node::create([
        'type'                     => $type,
        'title'                    => $title,
        'news_date_from'           => $date,
        'parse_news_field_related' => $parse_news_field_related,
        'body'                     => $body,
        'field_image'              => [
          'target_id' => $fid ? $fid : NULL,
        ],
      ]);
      $node->save();
      drupal_set_message(t('News added!'));
    }

    $form_state->setRedirect('parse_news.list');
  }

}
