<?php

namespace Drupal\parse_news\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Form\FormStateInterface;

use Drupal\parse_news\Controller\XMLBatchImport;

/**
 * Class ParserSettingForm.
 *
 * @package Drupal\parse_news\Form
 */
class ParserSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'parse_news.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, Request $request = NULL) {

    $config = $this->config('parse_news.settings');

    $form['amount_at_once'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Amount news'),
      '#description'   => $this->t('Amount of news for parsing from a recourse'),
      '#default_value' => $config->get('parse_news.amount_at_once'),
    ];

    $form['excerpt_lenght'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Length excerpts'),
      '#description'   => $this->t('Length excerpts'),
      '#default_value' => $config->get('parse_news.excerpt_lenght'),
    ];

    $form['parser_feed'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Feed Url'),
      '#description'   => $this->t('Feed Url'),
      '#default_value' => $config->get('parse_news.parser_feed'),
    ];

    $form['update_interval'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Update interval'),
      '#description'   => $this->t('The length of time between feed updates. Requires a correctly configured cron maintenance task.'),
      '#options'       => [
        '1800'   => t('30 min'),
        '3600'   => t('1 hour'),
        '210600' => t('6 hour'),
        '43200'  => t('12 hour'),
        '86400'  => t('1 day'),
      ],
      '#default_value' => $form_state->getValue('update_interval') ? $form_state->getValue('update_interval') : $config->get('parse_news.update_interval'),
    ];

    $form['actions']['start_import'] = [
      '#type'   => 'submit',
      '#value'  => $this->t('Start import'),
      '#submit' => ['::startImport'],
      '#weight' => 100,
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public static function startImport(array &$form, FormStateInterface $form_state) {
    $import = new XMLBatchImport();
    if (!empty($import->getData())) {
      $import->setBatch();
    }
    else {
      drupal_set_message('No new news.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
    $feed = UrlHelper::isValid($form_state->getValue('parser_feed'));

    if (!$feed) {
      drupal_set_message('Enter Valid URL!');
    }

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    parent::submitForm($form, $form_state);

    $values = $form_state->getValues();
    $this->config('parse_news.settings')
         ->set('parse_news.amount_at_once', $values['amount_at_once'])
         ->save();
    $this->config('parse_news.settings')
         ->set('parse_news.excerpt_lenght', $values['excerpt_lenght'])
         ->save();
    $this->config('parse_news.settings')
         ->set('parse_news.parser_feed', $values['parser_feed'])
         ->save();

    $configEditable = \Drupal::service('config.factory')
                             ->getEditable('parse_news.settings');
    $configEditable->set('parse_news.update_interval', $values['update_interval'])
                   ->save();


    return parent::submitForm($form, $form_state);
  }

}
