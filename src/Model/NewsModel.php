<?php

/**
 * Custom queries for the parse_news bundle.
 */

namespace Drupal\parse_news\Model;

use Drupal\Core\Database\Connection;
Use \Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Controller\ControllerBase;

/**
 * Class NewsModel.
 *
 * @package Drupal\parse_news\Model.
 */
class NewsModel extends ControllerBase {
  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityManager;

  /**
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityManager, Connection $connection) {
    $this->entityManager = $entityManager;
    $this->connection    = $connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity.manager'),
      $container->get('database')
    );
  }

  public function getEntity($entity) {
    return $this->entityManager->getStorage($entity);
  }

  /**
   * Load data for header to view list news in the admin pannel.
   *
   * @param array|NULL $header
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadHeader(array $header = NULL) {

    $query = $this->connection->select('node_field_data', 'n');
    $query->condition('type', 'parse_news');
    $query->fields('n', ['nid', 'title', 'created']);

    if ($header) {
      $table_sort = $query->extend('Drupal\Core\Database\Query\TableSortExtender')
                          ->orderByHeader($header);
      $pager      = $table_sort->extend('Drupal\Core\Database\Query\PagerSelectExtender');
      $result     = $pager->execute()->fetchAll();
    }
    else {
      $result = $query->execute()->fetchAll();
    }

    return $result;
  }

  /**
   * Delete news.
   *
   * @param int|string $id
   */
  public function delete($id) {
    $this->connection->delete('node_field_data')
                     ->condition('nid', $id)
                     ->execute();
  }

  /**
   * Load all bundles - parse_news.
   *
   * @param int $count
   *   Count of news.
   *
   * @return \Drupal\Core\Database\StatementInterface|null
   */
  public function loadNews($count = NULL, $tag = NULL) {
    $query = $this->connection->select('node_field_data', 'n');
    $query->fields('n', ['nid', 'title']);
    $query->condition('type', 'parse_news');
    $query->condition('status', 1);

    if ($count) {
      $query->range(0, $count);
    }

    if ($tag) {
      $term = $this->entityManager->getStorage('taxonomy_term')
                                  ->loadByProperties([
                                    'name' => $tag,
                                    'vid'  => 'parse_news_category',
                                  ]);
      $query->innerJoin('node__parse_news_field_related', 'fr', key($term) . ' = fr.parse_news_field_related_target_id');
      $query->where('fr.entity_id=n.nid');

    }

    $query->orderBy('created', 'DESC');
    $result = $query->execute();

    return $result;
  }

  /**
   * Load additional information(image src).
   *
   * @param int|string $id
   *   Id of the node.
   *
   * @return array
   */
  public function getEditValue($id) {

    $news = [];
    if (isset($id)) {
      $node = $this->entityManager->getStorage('node')
                                  ->load($id);

      $news['nid']                      = $id;
      $news['title']                    = $node->get('title')->value;
      $news['body']                     = $node->get('body')->value;
      $news['news_date_from']           = $node->get('news_date_from')->value;
      $news['file']                     = $node->get('field_image')
                                               ->getValue();
      $news['img_src']                  = $this->getImgUrl($news['file']);
      $news['parse_news_field_related'] = $node->get('parse_news_field_related')
                                               ->getValue();
    }

    return $news;
  }

  /**
   * Convert array of ids of the terms to the array objects of the terms.
   *
   * @param array $values
   *   Array contain ids of the terms.
   *
   * @return array
   *   Array of objects.
   */
  public function getEditRelatedValues(array $values) {
    $result = [];
    foreach ($values as $value) {
      $result[] = $this->entityManager
        ->getStorage('taxonomy_term')
        ->load($value['target_id']);
    }

    return $result;
  }

  /**
   * Returns url of the attached image to use in img scr tag.
   *
   * @param array $news_file_info
   *   Array contain info about image that attached to the node.
   *
   * @return \Drupal\Core\GeneratedUrl|string
   */
  public function getImgUrl(array $news_file_info) {

    if (isset($news_file_info[0]['target_id'])) {
      $news['file']    = $news_file_info[0]['target_id'];
      $news['img_src'] = $this->entityManager->getStorage('file')
                                             ->load($news['file']);
      $path            = $news['img_src']->getFileUri();
      $news['img_src'] = $this->entityManager->getStorage('image_style')
                                             ->load('medium')
                                             ->buildUrl($path);

      return $news['img_src'];
    }

    return FALSE;
  }

}
