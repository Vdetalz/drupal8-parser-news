<?php

namespace Drupal\parse_news\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Link;

use Drupal\parse_news\Model\NewsModel;

/**
 * Provides a 'LastNews' Block.
 *
 * @Block(
 *   id = "parser_news_block",
 *   admin_label = @Translation("Parser News block"),
 * )
 */
class ParserNewsBlock extends BlockBase implements BlockPluginInterface, ContainerFactoryPluginInterface {

  /**
   * @var \Drupal\parse_news\Model\NewsModel
   */
  private $newsModel;

  /**
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $configFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, NewsModel $newsModel, ConfigFactory $configFactory) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->newsModel     = $newsModel;
    $this->configFactory = $configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('parse_news.model'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $config = $this->getConfiguration();

    if (!empty($config['count'])) {
      $count = $config['count'];
    }
    else {
      $count = 1;
    }

    $result = $this->newsModel->loadNews($count);
    $items  = [];

    foreach ($result as $row) {
      $node         = $this->newsModel->getEntity('node')->load($row->nid);
      $path         = '/news-excerpt/' . $row->nid;
      $title        = Url::fromUri('internal:' . $path);
      $link_options = [
        'attributes' => [
          'class' => [
            'use-ajax',
          ],
        ],
      ];
      $title->setOptions($link_options);
      $title_link = Link::fromTextAndUrl($row->title, $title)->toString();

      $date = $node->get('news_date_from')->value;

      $items[] = [
        'title'      => $title_link,
        'date_parse' => $date,
        'class'      => 'insert' . $row->nid,
      ];
    }

    return [
      '#theme' => 'parser_news_block',
      '#items' => $items,
    ];
  }

  /**
   * {@inheritdoc}
   */
  protected function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIfHasPermission($account, 'administer blocks');
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'count'          => 1,
      'excerpt_lenght' => $this->getConfigFromConfig('excerpt_lenght'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['count'] = [
      '#type'          => 'number',
      '#min'           => 1,
      '#title'         => t('How many news display'),
      '#default_value' => $config['count'],
    ];

    $form['excerpt_lenght'] = [
      '#type'          => 'number',
      '#title'         => $this->t('Length excerpts'),
      '#description'   => $this->t('Length excerpts'),
      '#default_value' => $this->getConfigFromConfig('excerpt_lenght') ? $this->getConfigFromConfig('excerpt_lenght') : $config['excerpt_lenght'],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockValidate($form, FormStateInterface $form_state) {
    $count = $form_state->getValue('count');

    if (!is_numeric($count) || $count < 1) {
      $form_state->setErrorByName('count', t('Needs to be an interger and more or equal 1.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['count']          = $form_state->getValue('count');
    $this->configuration['excerpt_lenght'] = $form_state->getValue('excerpt_lenght');
    $this->setConfigFromBlock('excerpt_lenght', $form_state->getValue('excerpt_lenght'));
  }

  /**
   * Get config values.
   *
   * @return array
   */
  protected function getConfigFromConfig($config) {
    $config      = trim($config);
    $config_last = \Drupal::config('parse_news.settings');
    $valueConfig = $config_last->get('parse_news.' . $config);

    return $valueConfig;
  }

  /**
   * Set config values in the block and in the config store.
   *
   * @param $config
   * @param $value
   */
  protected function setConfigFromBlock($config, $value) {
    $config = trim($config);
    $value  = trim($value);

    $configEditable = $this->configFactory
      ->getEditable('parse_news.settings');
    $configEditable->set('parse_news.' . $config, $value)
                   ->save();
  }

}
